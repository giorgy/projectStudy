import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ThingsList } from '../../model/things-list';
import { Storage } from '@ionic/storage';

@Injectable()
export class ListProvider {

  constructor(
    public http: HttpClient,
    private storage: Storage
  ) {}

  public insert(thingsList: ThingsList){
    this.storage.set(thingsList.id, thingsList)
    this.storage.set('thingList',[{'id':thingsList.id, 'thingList':thingsList}])
  }

  public update() {}

  public save() {}

  public getAll(){
    let thingsList: Array<ThingsList>=[]
  
    return this.storage.forEach((list:ThingsList) => {
      thingsList.push(list);
    }).then(()=>{
      return Promise.resolve(thingsList);

    }).catch((erro)=>{
      return Promise.reject(erro)
    })
  }

  public remove(id){
    this.storage.remove(id)
  }
}
