import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Category } from '../../model/category';
import { Observer } from 'rxjs/Observer';


/*
  Generated class for the CategoryProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CategoryProvider {

  constructor(
    public http: HttpClient,
    private storage: Storage) { }


  public insert(category: Category) {
    //this.storage.set(category.id, category)

    //Criar ou Atualiza Lista de categorias
    this.storage.get('categories')
      .then(listCategory => {
        let list = JSON.parse(listCategory);
        
        if (list==null)
          {list = []}
        
          list.push({ 'id': category.id, 'name': category.name })
        this.storage.set('categories', JSON.stringify(list));
    
    }).catch(error => {
      console.log("Erro ao Salvar Banco" + error)
    })
  }
  

  public update(category: Category) { 
    this.storage.remove(category.id)
    this.storage.set(category.id, category)
  }

  public save() { }

  public getAll() {
    let listCategory: Array<Category> = []

    if (!this.storage.get('categories')){
      return this.storage.get('categories')
      .then(listCategoryString => {
        JSON.parse(listCategoryString)
        .forEach(category => {
          listCategory.push(category)
    })}).then(() => {
        return Promise.resolve(listCategory);

      }).catch((erro) => {
        console.log(erro)
        return Promise.reject(erro)
    })
    }
  }

  public getAllSubs() {
    let listCategory: Array<Category> = []

    return this.storage.forEach((category: Category) => {
      listCategory.push(category)});
      
  }

  public remove(id) {
    this.storage.remove(id)
  }
}