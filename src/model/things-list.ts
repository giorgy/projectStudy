import { Item } from "./item";
import { Category } from "./category";


export class ThingsList{
    id:string
    name:string
    category: Category
    date:Date
    status:boolean
    itens:Array<Item>

    constructor(
        name:string, 
        date:Date, 
        category: Category,
        itens:Array<Item>,
        status?, 
        id?
    )
        
        {
        
            if(id == null) {
            this.id = Date.now().toString();
        }

            if (status == null) {
                this.status = true;
        }

        this.name = name
        this.date = date
        this.category = category
        this.itens = itens
    }


    private setItemList(item:Item){
        this.itens.push(item)
    }

    private getItemList(index){
        return this.itens[index]
    }

    private deleteItemList(index){
        this.itens.splice(index,1)
    }
     
}