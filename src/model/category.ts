export class Category {
    id: string
    name: string

    constructor(name: string, id?) {
        if (id == null) {
            id = Date.now().toString();
        }

        this.name = name
        this.id = id
        
    }
}