import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';


import { ListProvider } from '../providers/list/list';

import { CategoryProvider } from '../providers/category/category';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NavMenuComponent } from '../components/nav-menu/nav-menu';
import { ComponentsModule } from '../components/components.module';
import { CategoryPageModule } from '../pages/category/category.module';
import { ThingListPageModule } from '../pages/thing-list/thing-list.module';


@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ComponentsModule,
    CategoryPageModule,
    ThingListPageModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ListProvider,
    CategoryProvider
  ]
})
export class AppModule {}
