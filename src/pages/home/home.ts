import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { CategoryPage } from '../category/category'



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  titlePage:string = 'Listas'

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController) {}



}
