import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertController } from 'ionic-angular';

import { ThingsList } from '../../model/things-list';
import { ListProvider } from '../../providers/list/list';
import { CategoryProvider } from '../../providers/category/category';
import { Category } from '../../model/category';
import { Item } from '../../model/item';



/**
 * Generated class for the ThingListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-thing-list',
  templateUrl: 'thing-list.html',
})
export class ThingListPage {

  titlePage: string = 'Nova Lista'
  protected formThinsgList: FormGroup;
  protected formListItem: FormGroup;

  protected load: any
  protected categories: Array<Category>
  protected itens: Array<Item> = []

  protected messageName = ""
  protected errorName = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private listProvider: ListProvider,
    private category: CategoryProvider,
    private loading: LoadingController,
    private alertCtrl: AlertController) {

    this.load = this.loading.create({ content: "loading..." })

    let today = new Date();
    let dateVal = today.getDate().toString() + '-' + (today.getMonth() + 1).toString() + '-' + today.getFullYear().toString()      

    //Form do Cabeçalho da lista
    this.formThinsgList = this.formBuilder.group({
      id: [''],
      name: ['', [
        Validators.required,
        Validators.pattern('[a-zA-Z ]*'),
        Validators.minLength(3),
        Validators.maxLength(10)
      ]],
      category: ['Categoria',[
        Validators.required,
      ]],
      date: [dateVal,[Validators.required]]
    });

    //Form da Lsita de coisas
    this.formListItem = this.formBuilder.group({
      item: ['', [
        Validators.required]]
    });

  }

  ionViewDidEnter() {
    this.category.getAll().then((categories) => {
      this.categories = categories
    })
  }


  private addItemList(){
    let field = this.formListItem.get('item').value;
    let item = new Item(field)
    this.itens.push(item)
    this.formListItem.reset()
    console.log(this.itens)
  }

  public removeItem(id, index) {
    this.itens.splice(index, 1)
  }

  protected createThingList() {
    this.load.present()
    let name = this.formThinsgList.get('name').value;
    let category = this.formThinsgList.get('category').value;
    let date = this.formThinsgList.get('date').value;
    let itens = this.itens
    let newThingsList = new ThingsList(name, date, category, itens)
    console.log(newThingsList)
    this.listProvider.insert(newThingsList);

    this.load.dismiss()
    this.formThinsgList.reset()
    this.formListItem.reset()
    this.navCtrl.pop()
  }

}
