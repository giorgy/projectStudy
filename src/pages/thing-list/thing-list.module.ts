import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ThingListPage } from './thing-list';

@NgModule({
  declarations: [
    ThingListPage,
  ],
  imports: [
    IonicPageModule.forChild(ThingListPage),
  ],
})
export class ThingListPageModule {}
