import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Category } from '../../model/category';
import { CategoryProvider } from '../../providers/category/category';
import { AlertController } from 'ionic-angular';


/**
 * Generated class for the CategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class CategoryPage {

  titlePage: string = 'Cadsatro de Categorias'
  protected formCategory: FormGroup;
  protected categories:Array<Category>
  protected load:any

  protected messageName = ""
  protected errorName = false;
 
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private category: CategoryProvider,
    private loading: LoadingController,
    private alertCtrl: AlertController) 
    {
      this.load = this.loading.create({ content: "loading..." })
      
    this.formCategory = this.formBuilder.group({
      id: [''],  
      name: ['', [
          Validators.required,
          Validators.pattern('[a-zA-Z ]*'), 
          Validators.minLength(3), 
          Validators.maxLength(10)
        ]]
      });
    }

  ionViewDidEnter() {
     this.category.getAll().then((categories) => {
       this.categories = categories
      
     })
  }

  protected creteCategory() {
    let { name } = this.formCategory.controls;
    //Define update or created
    if (this.formCategory.get('id').value == ""){

      //valitation field
      if (!this.formCategory.valid) {

        //define massenger error
        if(!name.valid){
          this.errorName = true;
          this.messageName = "Ops! Nome de Categoria inválido";
        } else {
          this.messageName = "";
          this.errorName = false;
        }
        
      } else {
        //cadastro
        this.load.present()
        let name = this.formCategory.get('name').value;
        let newCategory = new Category(name)
        
        this.category.insert(newCategory);
        
        this.load.dismiss()
        this.presentAlert("Categoria", "Cadastro Realizado!")
        this.formCategory.reset()
        this.navCtrl.pop()

      }
    }else{
      //update
      this.load.present()
      let id = this.formCategory.get('id').value;
      let name = this.formCategory.get('name').value;

      let updateCategory = new Category(name,id)
      
      this.category.update(updateCategory);
      this.load.dismiss()
      this.presentAlert("Categoria", "Atualização realizada")
      let result = this.categories.findIndex(catIndex => catIndex.id === updateCategory.id)
      this.categories[result] = updateCategory
      this.formCategory.reset()
  
    }

  }

 public presentAlert(titleAlert, message) {
    let alert = this.alertCtrl.create({
      title: titleAlert,
      subTitle: message,
      buttons: ['Ok']
    });
    alert.present();
  }


  public removeCategory(id, index){
    this.categories.splice(index,1)
    this.category.remove(id)
    this.presentAlert("Categoria", "Removido com sucesso!")
    this.formCategory.reset()
  }

  public editCategory (id, index){
    let category = this.categories[index]
    this.formCategory.patchValue(category);
    
  }

}
