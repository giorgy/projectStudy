import { NgModule } from '@angular/core';
import { NavMenuComponent } from './nav-menu/nav-menu';
import { IonicModule } from 'ionic-angular';
import { NavMenuHeaderComponent } from './nav-menu-header/nav-menu-header';
@NgModule({
	declarations: [NavMenuComponent,
    NavMenuHeaderComponent],
	imports: [IonicModule,],
	exports: [NavMenuComponent,
    NavMenuHeaderComponent]
})
export class ComponentsModule {}
