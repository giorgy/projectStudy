import { Component } from '@angular/core';
import { MenuController, NavController } from 'ionic-angular';

/**
 * Generated class for the NavMenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'nav-menu',
  templateUrl: 'nav-menu.html'
})
export class NavMenuComponent {

  text: string;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController) { }

  openMenu() {
    this.menuCtrl.open();
  }

  closeMenu() {
    this.menuCtrl.close();
  }

  toggleMenu() {
    this.menuCtrl.toggle();
  }

  public openPage(page) {
    this.navCtrl.push(page)
  }

  // public rootPage(){
  //   this.navCtrl.popToRoot()
  // }

}
