import { Component, Input } from '@angular/core';
import { MenuController, NavController } from 'ionic-angular';

/**
 * Generated class for the NavMenuHeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'nav-menu-header',
  templateUrl: 'nav-menu-header.html'
})
export class NavMenuHeaderComponent {

  @Input() title: string;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController) { }
}
