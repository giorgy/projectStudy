import { Directive } from '@angular/core';

/**
 * Generated class for the CardsDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
@Directive({
  selector: '[cards]' // Attribute selector
})
export class CardsDirective {

  constructor() {
    console.log('Hello CardsDirective Directive');
  }

}
