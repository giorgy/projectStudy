import { NgModule } from '@angular/core';
import { CardsDirective } from './cards/cards';
@NgModule({
	declarations: [CardsDirective],
	imports: [],
	exports: [CardsDirective]
})
export class DirectivesModule {}
